var isWithinBottom = function (clientHeight, scrollPos, offsetHeight, limit) {
  if (clientHeight + scrollPos >= offsetHeight - limit) {
    return true;
  }
  return false;
};

var addDiv = function () {
  var elemDiv = document.createElement('div');
  elemDiv.setAttribute('id', curPage);
  elemDiv.style.cssText = 'min-height:800px;';
  document.body.appendChild(elemDiv);
};

var addP = function (parent, content) {
  var elemP = document.createElement('p');
  elemP.textContent = content;
  parent.appendChild(elemP); 
};

var PROCESSING = false;
var curPage = 1;

window.onscroll = function(e) {
  
  var windowHeight = document.documentElement.clientHeight;
  var offsetHeight = document.getElementsByTagName('html')[0].offsetHeight;
  var scrollTop = document.documentElement.scrollTop;

  if (isWithinBottom(windowHeight, scrollTop, offsetHeight, 100)) {
    if (!PROCESSING) {
      PROCESSING = true;
      var ajaxRequest = new XMLHttpRequest(); 

      ajaxRequest.onreadystatechange = function(){
        if (ajaxRequest.readyState == 4) {
          var content = JSON.parse(ajaxRequest.responseText);
          for (i = 0; i < content.content.length; i++) {
            addP(document.getElementById(curPage), content.content[i]);
          }          
          PROCESSING = false;
          curPage += 1;
        }
      };
      addDiv();
      ajaxRequest.open("GET", "/?segment=" + curPage, true);
      ajaxRequest.setRequestHeader("X_REQUESTED_WITH", 'XMLHttpRequest'); 
      ajaxRequest.send(null);        
    } 
  }
};
