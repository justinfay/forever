"""
A forever scrolling app.
"""
from flask import (
    Flask,
    request,
    render_template,
    jsonify)


app = Flask(__name__)


PER_REQUEST = 10
LIPSUM = """Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sit amet augue sodales, pretium libero at, pellentesque libero. Nullam sollicitudin est eget eros molestie, a auctor ante vehicula. Nulla facilisi. Phasellus ullamcorper imperdiet egestas. Fusce eget mollis turpis. Proin ut elementum mauris, vel placerat felis. Quisque faucibus id eros id sagittis. Nullam at porttitor nisl, ut convallis libero. Vivamus eu hendrerit urna, sit amet ultrices tortor. Duis id diam erat. Nullam elementum, lectus at vestibulum hendrerit, magna enim sodales nulla, eget eleifend nunc massa at nisl. Nunc ornare, quam vitae feugiat vulputate, massa erat consequat nisi, et feugiat nunc elit eget purus. Cras non elit interdum justo dignissim eleifend quis vitae ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam elementum vestibulum massa, eget consectetur odio laoreet eu. Praesent a nibh volutpat, facilisis ante eu, pharetra lacus. """


@app.route('/')
def index():
    """
    Our one page forever scrolling app.
    """
    try:
        count = int(request.args.get('segment', 1))
    except ValueError:
        count = 1

    cur_max = count * PER_REQUEST
    content = [
        str(i) + ' ' + LIPSUM
        for i in xrange(cur_max - PER_REQUEST, cur_max)
    ]

    if request.is_xhr:
        return jsonify(content=content)

    return render_template('index.html', content=content)


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
